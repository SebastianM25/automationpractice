package Base;

import org.apache.commons.lang3.SystemUtils;

public class DriversUtils {
    public static void setWebDriveProperty(String browser) {
        if ("chrome".equals(browser)) {
            System.setProperty("webdriver.chrome.driver", getDriversPath() + "chromedriver" + getFileExtension());
        } else if ("firefox".equals(browser)) {
            System.setProperty("webdriver.gecko.driver", getDriversPath() + "geckodriver" + getFileExtension());
        }
    }

    private static String getProjectPath() {
        return System.getProperty("user.dir");
    }

    private static String getDriversPath() {
        String driversPath;
        if (SystemUtils.IS_OS_WINDOWS) {
            driversPath = "\\src\\main\\resources\\drivers\\windows";

        } else {

            driversPath = "/src/resources/drivers/mac/";
        }
        return getProjectPath() + driversPath;
    }

    private static String getFileExtension() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return ".exe";
        }
        return "";
    }

    String getDriverDirPath() {
        return System.getProperty("user.dir") + "/src/main/resources/drivers/" + this.getDriverDir();
    }

    private String getDriverDir() {
        return (SystemUtils.IS_OS_WINDOWS) ? "windows/" : "mac/";
    }

    String getDriverExtension() {
        return (SystemUtils.IS_OS_WINDOWS) ? ".exe" : "";
    }
}
