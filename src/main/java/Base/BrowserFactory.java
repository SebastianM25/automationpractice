package Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserFactory {
    public BrowserFactory(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver driver;

    public static WebDriver getDriver(String browserName) {
        DriversUtils.setWebDriveProperty(browserName);
        if ("chrome".equals(browserName)) {
            return new ChromeDriver();
        } else if ("firefox".equals(browserName)) {
            return new FirefoxDriver();
        } else if ("ie".equals(browserName)) {
            return new InternetExplorerDriver();
        }
        throw new RuntimeException("This browser is not supported ");
    }
}
