package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class Checkbox {

    WebDriver driver;
    private static final By checkbox1 = By.id("checkbox1");
    private static final By buttonsMenu = By.id("buttons");
    private static final By checkboxesMenu = By.id("checkboxes");

    public Checkbox(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnButtonsMenu() {
        driver.findElement(buttonsMenu).click();
    }

    public void clickOnCheckBoxesMenu() {
        driver.findElement(checkboxesMenu).click();
    }

    public void clickOnCheckbox() {
        driver.findElement(checkbox1).click();
    }




}
