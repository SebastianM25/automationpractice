package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    WebDriver driver;
    public LoginPage(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(id="email")
    private WebElement emailField;

    @FindBy(id="password")
    private  WebElement passwordField;

    public WebElement fillEmailField(){
        return emailField;
    }
    public WebElement fillPasswordFiled(){
        return passwordField;
    }
}
