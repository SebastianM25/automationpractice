package Page;

import Utilities.TextResources;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterFrom {

    WebDriver driver;
    TextResources textResources = new TextResources();

    public RegisterFrom(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "forms")
    private WebElement formSection;

    public WebElement clickOnFormSection() {
        return formSection;
    }

    @FindBy(id = "register")
    private WebElement registerSection;

    public WebElement clickOnregisterField() {
        return registerSection;
    }

    @FindBy(id = "firstName")
    private WebElement firstNameField;
    public WebElement firstNameFill() {
        return firstNameField;
    }

    @FindBy(id="lastName")
    private WebElement lastNameField;
    public WebElement lastNameFill(){
        return  lastNameField;
    }
    @FindBy(id="phone")
    private WebElement phoneNumer;
    public WebElement phoneFieldFill(){
        return phoneNumer;
    }
}
