package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Pagination {

    WebDriver driver;

    public Pagination(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(id = "pagination")
    private WebElement paginationPage;
    @FindBy(xpath = "//a[contains(text(),'1')]")
    private WebElement button_1;


    public WebElement clickOnPaginationBtn() {
        return paginationPage;
    }
    public WebElement clickOnButton_1(){
        return button_1;
    }


}
