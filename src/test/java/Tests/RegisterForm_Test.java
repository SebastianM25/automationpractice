package Tests;

import Base.BaseClass;
import Page.RegisterFrom;
import Utilities.TextResources;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class RegisterForm_Test extends BaseClass {
    TextResources textResources = new TextResources();

    @Test
    public void registerFormTest() throws InterruptedException {
        RegisterFrom registerFrom = new RegisterFrom(driver);

        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        registerFrom.clickOnFormSection().click();
        registerFrom.clickOnregisterField().click();

        registerFrom.firstNameFill().sendKeys(textResources.firstname);
        registerFrom.lastNameFill().sendKeys(textResources.lastname);
        registerFrom.phoneFieldFill().sendKeys(textResources.phoneNumber);
        Thread.sleep(3000);
    }
}
