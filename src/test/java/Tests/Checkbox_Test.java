package Tests;

import Base.BaseClass;
import Base.BrowserFactory;
import Page.Checkbox;
import org.junit.Test;

public class Checkbox_Test extends BaseClass {


    @Test
    public void testCheckboxElement() throws InterruptedException {
        int i = 0;
        for (i = 0; i < 2; i++) {
            Checkbox checkbox = new Checkbox(driver);
            checkbox.clickOnButtonsMenu();
            checkbox.clickOnCheckBoxesMenu();
            checkbox.clickOnCheckbox();
        }


        Thread.sleep(4000);
    }

    public void redirectPage() {
        driver.get("https://qa-automation-practice.netlify.app/checkboxes.html");
    }


}
