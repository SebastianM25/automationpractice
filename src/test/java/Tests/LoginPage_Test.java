package Tests;

import Base.BaseClass;
import Page.LoginPage;
import Utilities.TextResources;
import org.junit.Test;

public class LoginPage_Test extends BaseClass {
    TextResources resources=new TextResources();

    @Test
    public void LoginPage_Test(){
        LoginPage loginPage=new LoginPage(driver);
        loginPage.fillEmailField().sendKeys(resources.email);
        loginPage.fillPasswordFiled().sendKeys(resources.password);
    }

}
